import collections


class BSTNode:
    
    def __init__(self, key, value, count):
        self.key = key
        self.value = value
        self.left = None
        self.right = None
        self.count = count
        
    def __repr__(self):
        return '{}: {}'.format(self.key, self.value)
        
    def __str__(self):
        return '{}: {}'.format(self.key, self.value)


class BST:
    
    def __init__(self):
        self._root = None
        
    def is_empty(self):
        return len(self) == 0

    def __len__(self):
        return self._size(self._root)
        
    def _size(self, x):
        if x is None:
            return 0
        return x.count
        
    # Search
    def __contains__(self, key):
        try:
            return self[key] is not None
        except KeyError:
            return False

    def __getitem__(self, key):
        return self._get(self._root, key)
    
    def _get(self, x, key):
        if x is None:
            raise KeyError(key)
        if key < x.key:
            return self._get(x.left, key)
        elif key > x.key:
            return self._get(x.right, key)
        else:
            return x.value
    
    # Insertion
    def __setitem__(self, key, value):
        self._root = self._put(self._root, key, value)
        
    def _put(self, x, key, value):
        if x is None:
            return BSTNode(key, value, 1)
        if key < x.key:
            x.left = self._put(x.left, key, value)
        elif key > x.key:
            x.right = self._put(x.right, key, value)
        else:
            x.value = value
        x.count = 1 + self._size(x.left) + self._size(x.right)
        return x

    # Deletion
    def delete_min(self):
        self._root = self._delete_min(self._root)

    def _delete_min(self, x):
        if x.left is None:
            return x.right
        x.left = self._delete_min(x.left)
        x.count = self._size(x.left) + self._size(x.right) + 1
        return x

    def delete_max(self):
        self._root = self._delete_max(self._root)

    def _delete_max(self, x):
        if x.right is None:
            return x.left
        x.right = self._delete_max(x.right)
        x.count = self._size(x.left) + self._size(x.right) + 1
        return x

    def __delitem__(self, key):
        self._root = self._delete(self._root, key)
        
    def _delete(self, x, key):
        if x is None:
             raise KeyError(key)
        if key < x.key:
            x.left = self._delete(x.left, key)
        elif key > x.key:
            x.right = self._delete(x.right, key)
        else:
            if x.right is None:
                return x.left
            if x.left is None:
                return x.right
            t = x
            x = self._min(t.right)
            x.right = self._delete_min(t.right)
            x.left = t.left
        x.count = self._size(x.left) + self._size(x.right) + 1
        return x

    # Min, max, floor, ceiling
    def min(self):
        if self.is_empty():
            raise ValueError
        return self._min(self._root).key
    
    def _min(self, x):
        if x.left is None:
            return x
        return self._min(x.left)

    def max(self):
        if self.is_empty():
            raise ValueError
        return self._max(self._root).key
    
    def _max(self, x):
        if x.right is None:
            return x
        return self._max(x.right)

    def floor(self, key):
        x = self._floor(self._root, key)
        if x is None:
            return None
        return x.key

    def _floor(self, x, key):
        if x is None:
            return None
        if key == x.key:
            return x
        if key < x.key:
            return self._floor(x.left, key)
        t = self._floor(x.right, key)
        if t is not None:
            return t
        else:
            return x
        
    def ceiling(self, key):
        x = self._ceiling(self._root, key)
        if x is None:
            return None
        return x.key

    def _ceiling(self, x, key):
        if x is None:
            return None
        if key == x.key:
            return x
        if key > x.key:
            return self._ceiling(x.right, key)
        t = self._ceiling(x.left, key)
        if t is not None:
            return t
        else:
            return x

    # Rank and selection
    def select(self, k):
        if (k < 0 or k >= len(self)):
            return None
        x = self._select(self.root, k)
        return x.key
        
    def _select(self, x, k):
        if x is None:
            return None
        t = self._size(x.left)
        if t > k:
            return self._select(x.left, k)
        elif t < k:
            return self._select(x.right, k-t-1)
        else:
            return x
        
    def rank(self, key):
        return self._rank(key, self._root)
        
    def _rank(self, key, x):
        if x is None:
            return 0
        if key < x.key:
            return self._rank(key, x.left)
        elif key > x.key:
            return self._size(x.left) + self._rank(key, x.right) + 1
        else:
            return self._size(x.left)
    
    # Range count and search
    def __iter__(self):
        return self.keys(self.min(), self.max())
    
    def keys(self, lo=None, hi=None):
        if lo is None:
            lo = self.min()
        if hi is None:
            hi = self.max()
        q = []
        self._keys(self._root, q, lo, hi)
        return (i for i in q)
        
    def _keys(self, x, q, lo, hi):
        if x is None:
            return
        if lo < x.key:
            self._keys(x.left, q, lo, hi)
        if lo <= x.key <= hi:
            q.append(x.key)
        if hi > x.key:
            self._keys(x.right, q, lo, hi)

    def size(self, lo, hi):
        if lo > hi:
            return 0
        if hi in self:
            return self.rank(hi) - self.rank(lo) +1
        else:
            return self.rank(hi) - self.rank(lo)
            
    def height(self):
        return self._height(self._root)
        
    def _height(self, x):
        if x is None:
            return -1
        return max(self._height(x.left), self._height(x.right)) + 1

    def level_order(self):
        kq = []
        nq = collections.deque()
        nq.appendleft(self._root)
        while nq:
            x = nq.pop()
            if x is None:
                continue
            kq.append(x.key)
            nq.appendleft(x.left)
            nq.appendleft(x.right)
        return (i for i in kq)

    # Other methods
    def values(self):
        pass
        
    def items(self):
        pass
        
    def get(self, key, default=None):
        pass
        
    def clear(self):
        pass
        
    def setdefault(self, key, default=None):
        pass
        
    def pop(self):
        pass
        
    def popitem(self):
        pass
        
    def copy(self):
        pass
        
    def update(self):
        pass


class RedBlackBSTNode:
    
    RED = 'R'
    BLACK = 'B'
    
    def __init__(self, key, value=None):
        self.key = key
        if value is None:
            self.value = True
        else:
            self.value = value
        self.left = None
        self.right = None
        self.count = 1
        self.color = RedBlackBSTNode.RED
        
    def __repr__(self):
        return '{}: {}'.format(self.key, self.value)
        
    def __str__(self):
        return '{}: {}'.format(self.key, self.value)


class RedBlackBST:
    
    def __init__(self):
        self._root = None
    
    def put(self, key, value=None):
        self._root = self._put(self._root, key, value)
        self._root.color = RedBlackBSTNode.BLACK
        
    def _put(self, h, key, value):
        if h is None:
            return RedBlackBSTNode(key, value)
        if key < h.key:
            h.left = self._put(h.left, key, value)
        elif key > h.key:
            h.right = self._put(h.right, key, value)
        else:
            h.value = value

        if self._is_red(h.right) and not self._is_red(h.left):
            h = self._rotate_left(h)
        if self._is_red(h.left) and self._is_red(h.left.left):
            h = self._rotate_right(h)
        if self._is_red(h.left) and self._is_red(h.right):
            self._flip_colors(h)
        
        return h

    def get(self, key):
        x = self._root
        while x is not None:
            if key < x.key:
                x = x.left
            elif key > x.key:
                x = x.right
            else:
                return x.value
        return None
        
    def min(self):
        if self._root is None:
            return None
        return self._min(self._root)
    
    def _min(self, x):
        while x.left is not None:
            x = x.left
        return x.key

    def max(self):
        if self._root is None:
            return None
        return self._max(self._root)
    
    def _max(self, x):
        while x.right is not None:
            x = x.right
        return x.key

    def floor(self, key):
        x = self._floor(self._root, key)
        if x is None:
            return None
        return x.key

    def _floor(self, x, key):
        if x is None:
            return None
        if key == x.key:
            return x
        if key < x.key:
            return self._floor(x.left, key)
        t = self._floor(x.right, key)
        if t is not None:
            return t
        else:
            return x
        
    def ceiling(self, key):
        x = self._ceiling(self._root, key)
        if x is None:
            return None
        return x.key

    def _ceiling(self, x, key):
        if x is None:
            return None
        if key == x.key:
            return x
        if key > x.key:
            return self._ceiling(x.right, key)
        t = self._ceiling(x.left, key)
        if t is not None:
            return t
        else:
            return x

    def _is_red(self, x):
        if x is None:
            return False
        return x.color == RedBlackBSTNode.RED

    def _rotate_left(self, h):
        assert self._is_red(h.right)
        x = h.right
        h.right = x.left
        x.left = h
        x.color = h.color
        h.color = RedBlackBSTNode.RED
        return x

    def _rotate_right(self, h):
        assert self._is_red(h.left)
        x = h.left
        h.left = x.right
        x.right = h
        x.color = h.color
        h.color = RedBlackBSTNode.RED
        return x
        
    def _flip_colors(self, h):
        assert not self._is_red(h)
        assert self._is_red(h.left)
        assert self._is_red(h.right)
        h.color = RedBlackBSTNode.RED
        h.left.color = RedBlackBSTNode.BLACK
        h.right.color = RedBlackBSTNode.BLACK

    def color(self, key):
        x = self._root
        while x is not None:
            if key < x.key:
                x = x.left
            elif key > x.key:
                x = x.right
            else:
                return x.color
        return None

    def keys(self):
        q = []
        self._in_order(self._root, q)
        return (i for i in q)
        
    def _in_order(self, x, q):
        if x is None:
            return
        self._in_order(x.left, q)
        q.append(x.key)
        self._in_order(x.right, q)

    def level_order(self):
        q = collections.deque()
        q.appendleft(self._root)
        while q:
            x = q.pop()
            yield x.key
            if x.left is not None:
                q.appendleft(x.left)
            if x.right is not None:
                q.appendleft(x.right)
