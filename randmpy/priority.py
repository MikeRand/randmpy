"""
>>> tasks = [[float('inf'), 0], [float('inf'), 1], [float('inf'), 2]]
>>> pq = PriorityQueue(tasks)
>>> len(pq)
3
>>> pq.task_priority(2)
inf
>>> pq.add_task(2, 0)
>>> pq.task_priority(2)
0
>>> len(pq)
3
>>> pq.has_task(2)
True
>>> pq.pop_task()
2
>>> pq.has_task(2)
False
>>> pq.pop_task()
0
>>> pq.pop_task()
1
>>> len(pq)
0
>>> pq.pop_task()
Traceback (most recent call last):
...
KeyError: 'pop from an empty priority queue'

"""

import heapq
import itertools


class PriorityQueue:
    
    def __init__(self, tasks=None):
        self.pq = []
        heapq.heapify(self.pq)
        self.entry_finder = {}
        self.REMOVED = '<removed-task>'
        self.counter = itertools.count()
        self._len = 0
        
        if tasks is None:
            tasks = []
        for priority, task in tasks:
            self.add_task(task, priority)

    def has_task(self, task):
        return task in self.entry_finder

    def add_task(self, task, priority=0):
        if task in self.entry_finder:
            self.remove_task(task)
        count = next(self.counter)
        entry = [priority, count, task]
        self.entry_finder[task] = entry
        heapq.heappush(self.pq, entry)
        self._len += 1
        
    def remove_task(self, task):
        entry = self.entry_finder.pop(task)
        entry[-1] = self.REMOVED
        self._len -= 1
        
    def pop_task(self):
        while self.pq:
            priority, count, task = heapq.heappop(self.pq)
            if task is not self.REMOVED:
                del self.entry_finder[task]
                self._len -= 1
                return task
        raise KeyError('pop from an empty priority queue')
        
    def task_priority(self, task):
        return self.entry_finder[task][0]

    def __len__(self):
        return self._len

    def __str__(self):
        return str(self.pq)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
