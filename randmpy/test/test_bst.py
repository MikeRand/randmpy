import unittest

import randmpy.bst


class TestBST(unittest.TestCase):
    
    def setUp(self):
        self.tree = randmpy.bst.BST()
    
    def test_is_empty_true(self):
        self.assertTrue(self.tree.is_empty())

        self.tree['A'] = True
        self.assertFalse(self.tree.is_empty())
        
    def test_length(self):
        self.assertEqual(len(self.tree), 0)

        self.tree['A'] = True
        self.assertEqual(len(self.tree), 1)
        
        self.tree['B'] = True
        self.assertEqual(len(self.tree), 2)
    
    def test_contains(self):
        self.assertNotIn('A',self.tree)
        self.tree['A'] = True
        self.assertIn('A', self.tree)
        del self.tree['A']
        self.assertNotIn('A', self.tree)
        
    def test_put_and_get(self):
        self.assertRaises(KeyError, self.tree.__getitem__, 'A')
        self.tree['A'] = 'test'
        self.assertEqual(self.tree['A'], 'test')
        del self.tree['A']
        self.assertRaises(KeyError, self.tree.__getitem__, 'A')

    def test_delete_min_max(self):
        keys = ('F', 'A', 'Z')
        for key in keys:
            self.tree[key] = True
        self.assertTrue(self.tree['A'])
        self.tree.delete_min()
        self.assertRaises(KeyError, self.tree.__getitem__, 'A')
        self.assertTrue(self.tree['Z'])
        self.tree.delete_max()
        self.assertRaises(KeyError, self.tree.__getitem__, 'Z')

        
if __name__ == '__main__':
    unittest.main()
