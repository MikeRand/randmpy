import os, sys
sys.path.insert(0, os.getcwd())

import unittest

import mbrpylib.structures


class TestDisjointSet(unittest.TestCase):
    
    def setUp(self):
        self.ds = structures.DisjointSet()
        for i in range(10):
            self.ds.make_set(i)
            
    def test_init(self):
        for i in range(10):
            self.assertEqual(self.ds.find_set(i), i)
            
    def test_union(self):
        self.ds.union(2, 7)
        self.assertEqual(self.ds.find_set(2), self.ds.find_set(7))
        self.ds.union(3, 2)
        self.assertEqual(self.ds.find_set(3), self.ds.find_set(2))
        self.assertEqual(self.ds.find_set(3), self.ds.find_set(7))
        

if __name__ == '__main__':
    unittest.main()
