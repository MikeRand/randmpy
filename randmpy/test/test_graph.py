import unittest

import randmpy.graph


class TestGraph(unittest.TestCase):
    
    def setUp(self):
        self.g = randmpy.graph.Graph()
        
    def test_vertex_count_empty(self):
        self.assertEqual(self.g.vertex_count, 0)
        
    def test_edge_count_empty(self):
        self.assertEqual(self.g.edge_count, 0)
        
    def test_add_vertex(self):
        self.g.add_vertex(0)
        self.assertEqual(self.g.vertex_count, 1)
        self.assertEqual(self.g.edge_count, 0)
        
    def test_add_edge(self):
        self.g.add_edge(0, 1)
        self.assertEqual(self.g.vertex_count, 2)
        self.assertEqual(self.g.edge_count, 1)
        self.assertIn(1, self.g.out_adjacencies(0))
        self.assertIn(0, self.g.in_adjacencies(1))

if __name__ == '__main__':
    unittest.main()
