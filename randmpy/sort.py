"""
Merge Sort
>>> l = [5, 2, 3, 1, 4]
>>> s = msorted(l)
>>> l
[5, 2, 3, 1, 4]
>>> s
[1, 2, 3, 4, 5]
>>> msorted.inversions
6

>>> l = ['e', 'b', 'c', 'a', 'd']
>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> s = msorted(l, key=key)
>>> l
['e', 'b', 'c', 'a', 'd']
>>> s
['c', 'b', 'd', 'e', 'a']
>>> msorted.inversions
5

>>> l = [5, 2, 3, 1, 4]
>>> s = msorted(l, reverse=True)
>>> l
[5, 2, 3, 1, 4]
>>> s
[5, 4, 3, 2, 1]
>>> msorted.inversions
4

>>> l = ['e', 'b', 'c', 'a', 'd']

>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> s = msorted(l, key=key, reverse=True)
>>> s
['a', 'e', 'd', 'b', 'c']
>>> msorted.inversions
5

>>> l = [5, 2, 3, 1, 4]
>>> msort(l)
>>> l
[1, 2, 3, 4, 5]
>>> msort.inversions
6

>>> l = ['e', 'b', 'c', 'a', 'd']
>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> msort(l, key=key)
>>> l
['c', 'b', 'd', 'e', 'a']
>>> msort.inversions
5

>>> l = [5, 2, 3, 1, 4]
>>> msort(l, reverse=True)
>>> l
[5, 4, 3, 2, 1]
>>> msort.inversions
4

>>> l = ['e', 'b', 'c', 'a', 'd']
>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> msort(l, key=key, reverse=True)
>>> l
['a', 'e', 'd', 'b', 'c']
>>> msort.inversions
5

Quicksort
>>> l = [5, 2, 3, 1, 4]
>>> s = qsorted(l)
>>> l
[5, 2, 3, 1, 4]
>>> s
[1, 2, 3, 4, 5]
>>> qsorted.comparisons
6

>>> l = ['e', 'b', 'c', 'a', 'd']
>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> s = qsorted(l, key=key)
>>> l
['e', 'b', 'c', 'a', 'd']
>>> s
['c', 'b', 'd', 'e', 'a']
>>> qsorted.comparisons
6

>>> l = [5, 2, 3, 1, 4]
>>> s = qsorted(l, reverse=True)
>>> l
[5, 2, 3, 1, 4]
>>> s
[5, 4, 3, 2, 1]
>>> qsorted.comparisons
6

>>> l = ['e', 'b', 'c', 'a', 'd']

>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> s = qsorted(l, key=key, reverse=True)
>>> s
['a', 'e', 'd', 'b', 'c']
>>> qsorted.comparisons
6

>>> l = [5, 2, 3, 1, 4]
>>> qsort(l)
>>> l
[1, 2, 3, 4, 5]
>>> qsort.comparisons
6

>>> l = ['e', 'b', 'c', 'a', 'd']
>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> qsort(l, key=key)
>>> l
['c', 'b', 'd', 'e', 'a']
>>> qsort.comparisons
6

>>> l = [5, 2, 3, 1, 4]
>>> qsort(l, reverse=True)
>>> l
[5, 4, 3, 2, 1]
>>> qsort.comparisons
6

>>> l = ['e', 'b', 'c', 'a', 'd']
>>> d = {'a': 10,
...      'b': 4,
...      'c': 2,
...      'd': 5,
...      'e': 9}
>>> key = lambda x: d[x]
>>> qsort(l, key=key, reverse=True)
>>> l
['a', 'e', 'd', 'b', 'c']
>>> qsort.comparisons
6

"""

import operator
import random


class MergeSort:

    def __init__(self, inplace=False):
        self.inplace = inplace

    def __call__(self, l, key=None, reverse=False):

        self.inversions = 0
        
        if key is None:
            key = lambda x: x

        if reverse:
            compare = operator.gt
        else:
            compare = operator.lt

        if self.inplace:
            dest = l
        else:
            dest = list(l)

        working = [0] * len(l)
            
        self.inversions = self._merge_sort(dest, working, 0, len(dest),
                                           key, compare)

        if not self.inplace:        
            return dest

    def _merge_sort(self, dest, working, low, high, key, compare):

        if low < high - 1:
            mid = (low + high) // 2
            x = self._merge_sort(dest, working, low, mid, key, compare)
            y = self._merge_sort(dest, working, mid, high, key, compare)
            z = self._merge(dest, working, low, mid, high, key, compare)
            return (x + y + z)
        else:
            return 0

    def _merge(self, dest, working, low, mid, high, key, compare):

        i = 0
        j = 0
        inversions = 0

        while (low + i < mid) and (mid + j < high):
            if compare(key(dest[low + i]), key(dest[mid + j])):
                working[low + i + j] = dest[low + i]
                i += 1
            else:
                working[low + i + j] = dest[mid + j]
                inversions += (mid - (low + i))
                j += 1

        while low + i < mid:
            working[low + i + j] = dest[low + i]
            i += 1

        while mid + j < high:
            working[low + i + j] = dest[mid + j]
            j += 1

        for k in range(low, high):
            dest[k] = working[k]

        return inversions
                                        

msorted = MergeSort(inplace=False)
msort = MergeSort(inplace=True)


class QuickSort:
    
    def __init__(self, choose_pivot, inplace=True):
        self.choose_pivot = choose_pivot
        self.inplace = inplace

    def __call__(self, l, key=None, reverse=False):
        self.comparisons = 0

        if key is None:
            key = lambda x: x

        if reverse:
            compare = operator.gt
        else:
            compare = operator.lt

        if self.inplace:
            dest = l
        else:
            dest = list(l)

        self.comparisons = self._quick_sort(dest, 0, len(dest),
                                            key, compare)
        if not self.inplace:        
            return dest
        
    def _quick_sort(self, dest, low, high, key, compare):
        if low < high - 1:
            pivot = self.choose_pivot(dest, low, high, key, compare)
            mid, a = self._partition(dest, low, pivot, high, key, compare)
            b = self._quick_sort(dest, low, mid, key, compare)
            c = self._quick_sort(dest, mid + 1, high, key, compare)
            return a + b + c
        else:
            return 0

    def _partition(self, dest, low, pivot, high, key, compare):
        dest[low], dest[pivot] = dest[pivot], dest[low]
        i = low + 1
        for j in range(low + 1, high):
            if compare(key(dest[j]), key(dest[low])):
                dest[i], dest[j] = dest[j], dest[i]
                i += 1
        dest[low], dest[i-1] = dest[i-1], dest[low]
        return i - 1, (high - low - 1)
        

def choose_pivot_random(dest, low, high, key, compare):
    return random.randrange(low, high)


def choose_pivot_first(dest, low, high, key, compare):
    return low


def choose_pivot_last(dest, low, high, key, compare):
    return high - 1


def choose_pivot_midpoint(dest, low, high, key, compare):
    return (low + high) // 2


def choose_pivot_median(dest, low, high, key, compare):
    mid = (low + high - 1) // 2
    if compare(key(dest[low]), key(dest[mid])):
        if compare(key(dest[mid]), key(dest[high-1])):
            return mid
        elif compare(key(dest[low]), key(dest[high-1])):
            return high - 1
        else:
            return low
    else:
        if compare(key(dest[low]), key(dest[high-1])):
            return low
        elif compare(key(dest[mid]), key(dest[high-1])):
            return high - 1
        else:
            return mid


qsort = QuickSort(choose_pivot=choose_pivot_median, inplace=True)
qsorted = QuickSort(choose_pivot=choose_pivot_median, inplace=False)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
