import randmpy.bst
import randmpy.priority

def intersections_2d(lines):
    """Return iterator of tuples with horizontal/vertical line
    intersections."""
    pq = randmpy.priority.PriorityQueue()

    for line, points in lines.items():
        p1, p2 = points
        if p1[1] == p2[1]:
            min_x = min(p1[0], p2[0])
            max_x = max(p1[0], p2[0])
            pq.add_task((line, 'B'), min_x)
            pq.add_task((line, 'E'), max_x)
        else:
            pq.add_task((line, 'V'), p1[0])
    
    tree = randmpy.bst.BST()
    while pq:
        line, event = pq.pop_task()
        p1, p2 = lines[line]
        if event == 'B':
            tree[p1[1]] = line
        elif event == 'E':
            del tree[p1[1]]
        elif event == 'V':
            min_y = min(p1[1], p2[1])
            max_y = max(p1[1], p2[1])
            for key in tree.keys(min_y, max_y):
                crossing_line = tree[key]
                yield (line, crossing_line)
