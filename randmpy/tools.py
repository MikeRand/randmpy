def master_method(a, b, c):
    """Return complexity of recurrence 'T(n) = aT(n/b) + O(n ** c)' as string.

    Examples from Algorithms coursera course

    Merge sort
    >>> master_method(a=2, b=2, c=1)
    'O(n log(n))'

    Binary search in sorted array
    >>> master_method(a=1, b=2, c=0)
    'O(log(n))'

    Normal integer recursive multiplication
    >>> master_method(a=4, b=2, c=1)
    'O(n ** log2(4))'

    Gaussian recursive multiplication
    >>> master_method(a=3, b=2, c=1)
    'O(n ** log2(3))'

    Strassen's matrix multiplication
    >>> master_method(a=7, b=2, c=2)
    'O(n ** log2(7))'

    Fictitious example
    >>> master_method(a=2, b=2, c=2)
    'O(n ** 2)'

    """
    rhs = b ** c

    if a == rhs and c == 1:
        return 'O(n log(n))'
    elif a == rhs and c == 0:
        return 'O(log(n))'
    elif a == rhs:
        return 'O(n ** {} log(n))'.format(c)
    elif a < rhs and c == 1:
        return 'O(n)'
    elif a < rhs:
        return 'O(n ** {})'.format(c)
    elif a == b:
        return 'O(n)'
    elif a == 1:
        return 'O(n ** log({}))'.format(b)
    else:
        return 'O(n ** log{}({}))'.format(b, a)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
