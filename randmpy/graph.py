"""Graph algorithms."""

import itertools

import randmpy.priority


def dfs(vertex_ids, out_adjacencies):
    """Run depth-first search on graph.

    Args:
      vertex_ids      - iterable
      out_adjacencies - callable that takes a vertex id and returns
                        an iterable of vertex ids

    Returns: a 3-tuple
       dict - vertex: discovery times
       dict - vertex: finish times
       dict - vertex: predecessors
       dict - vertex: component id

    Example (from CLRS Chapter 22):
    >>> adjacencies = {'u': ['v', 'x'],
    ...                'v': ['y'],
    ...                'w': ['z'],
    ...                'x': ['v'],
    ...                'y': ['x'],
    ...                'z': ['z']}
    >>> out_adjacencies = lambda x: sorted(adjacencies[x])
    >>> vertices = sorted(adjacencies.keys())
    >>> discover, finish, parent, cc = dfs(vertices, out_adjacencies)
    >>> discover == {'u': 1, 'v': 2, 'w': 9, 'x': 4, 'y': 3, 'z': 10}
    True
    >>> finish == {'u': 8, 'v': 7, 'w': 12, 'x': 5, 'y': 6, 'z': 11}
    True
    >>> parent == {'u': None, 'v': 'u', 'w': None,
    ...            'x': 'y', 'y': 'v', 'z': 'w'} 
    True
    
    """
    color = {}
    d = {}
    f = {}
    p = {}
    cc = {}
    time = itertools.count(1)
    
    for u in vertex_ids:
        color[u] = 0
        p[u] = None
    
    def dfs_visit(vertex_ids, out_adjacencies, u, cc_id):
        cc[u] = cc_id
        d[u] = next(time)
        color[u] = 1
        for v in out_adjacencies(u):
            if color[v] == 0:
                p[v] = u
                dfs_visit(vertex_ids, out_adjacencies, v, cc_id)
        color[u] = 2
        f[u] = next(time)
    
    for u in vertex_ids:
        if color[u] == 0:
            dfs_visit(vertex_ids, out_adjacencies, u, u)
    
    return d, f, p, cc


def strongly_connected_components(vertex_ids, out_adjacencies):
    """Run depth-first search on graph.

    Args:
      vertex_ids      - iterable
      out_adjacencies - callable that takes a vertex id and returns
                        an iterable of vertex ids

    Returns:
       dict - vertex: component id

    Example (from CLRS Chapter 22):
    >>> adjacencies = {'a': ['b'],
    ...                'b': ['c', 'e', 'f'],
    ...                'c': ['d', 'g'],
    ...                'd': ['c', 'h'],
    ...                'e': ['a', 'f'],
    ...                'f': ['g'],
    ...                'g': ['f', 'h'],
    ...                'h': ['h']}
    >>> out_adjacencies = lambda x: sorted(adjacencies[x])
    >>> vertices = ['c', 'g', 'f', 'h', 'd', 'b', 'e', 'a']
    >>> cc = strongly_connected_components(vertices, out_adjacencies)
    >>> cc == {'a': 'b', 'b': 'b', 'c': 'c', 'd': 'c', 'e': 'b', 'f': 'g',
    ...        'g': 'g', 'h': 'h'}
    True

    """
    d, f, p, cc = dfs(vertex_ids, out_adjacencies)
    vertex_transpose = sorted(vertex_ids, key=lambda x: -1 * f[x])
    in_adjacencies = {}
    for vertex in vertex_ids:
        for adjacency in out_adjacencies(vertex):
            if adjacency not in in_adjacencies:
                in_adjacencies[adjacency] = set()
            in_adjacencies[adjacency].add(vertex)
    i = lambda x: in_adjacencies.get(x, [])
    del vertex_ids
    del out_adjacencies
    d, f, p, cc = dfs(vertex_transpose, i)
    return cc

# Single-Source Shortest Path (SSSP)

def bfs(vertex_ids, out_adjacencies, source):
    pass
    
def dijkstra(vertex_ids, out_adjacencies, weight, source):
    """Run Dijkstra SSSP algorithm on graph.
    
    Args:
      vertex_ids      - iterable
      out_adjacencies - callable that takes a vertex id and returns
                        an iterable of vertex ids
      weight          - callable that takes two vertex ids and returns
                        a float
      source          - the source vertex id
      
    Returns: a 2-tuple
       dict - destination: path_length
       dict - destination: predecessor

    Example (from CLRS Chapter 24):
    >>> adjacencies = {'s': ['t', 'y'],
    ...                't': ['x', 'y'],
    ...                'x': ['z'],
    ...                'y': ['t', 'x', 'z'],
    ...                'z': ['s', 'x']}
    >>> out_adjacencies = lambda x: sorted(adjacencies[x])
    >>> vertices = sorted(adjacencies.keys())
    >>> wgt = {('s', 't'): 10,
    ...        ('s', 'y'): 5,
    ...        ('t', 'x'): 1,
    ...        ('t', 'y'): 2,
    ...        ('x', 'z'): 4,
    ...        ('y', 't'): 3,
    ...        ('y', 'x'): 9,
    ...        ('y', 'z'): 2,
    ...        ('z', 's'): 7,
    ...        ('z', 'x'): 6}
    >>> weights = lambda x, y: wgt.get((x, y), float('inf'))
    >>> source = 's'
    >>> value, solution = dijkstra(vertices,
    ...                            out_adjacencies,
    ...                            weights,
    ...                            source)
    >>> value == {'s': 0, 't': 8, 'x': 9, 'y': 5, 'z': 7}
    True
    >>> solution == {'s': None, 't': 'y', 'x': 't', 'y': 's', 'z': 'y'} 
    True
    
    """
    d = {}
    predecessor = {}
    for v in vertex_ids:
        d[v] = float('inf')
        predecessor[v] = None
    d[source] = 0
    S = set()
    Q = randmpy.priority.PriorityQueue()
    for v in vertex_ids:
        Q.add_task(v, priority=d[v])
    while Q:
        u = Q.pop_task()
        S.add(u)
        for v in out_adjacencies(u):
            challenge = d[u] + weight(u, v)
            if d[v] > challenge:
                d[v] = challenge
                predecessor[v] = u
                Q.add_task(v, priority=d[v])
    return d, predecessor 


def bellman_ford(vertex_ids, out_adjacencies, weight, source):
    """Run Bellman-Ford SSSP algorithm on graph.
    
    Args:
      vertex_ids      - iterable
      out_adjacencies - callable that takes a vertex id and returns
                        an iterable of vertex ids
      weight          - callable that takes two vertex ids and
                        returns a float
      source          - the source vertex id
      
    Returns: a 3-tuple
       boolean - does graph contain a negative cycle?
       dict - destination: path_length
       dict - destination: predecessor
       
    Example (from CLRS Chapter 24):
    >>> adjacencies = {'s': ['t', 'y'],
    ...                't': ['x', 'y', 'z'],
    ...                'x': ['t'],
    ...                'y': ['x', 'z'],
    ...                'z': ['s', 'x']}
    >>> out_adjacencies = lambda x: sorted(adjacencies[x])
    >>> wgt = {('s', 't'): 6,
    ...        ('s', 'y'): 7,
    ...        ('t', 'x'): 5,
    ...        ('t', 'y'): 8,
    ...        ('t', 'z'): -4,
    ...        ('x', 't'): -2,
    ...        ('y', 'x'): -3,
    ...        ('y', 'z'): 9,
    ...        ('z', 's'): 2,
    ...        ('z', 'x'): 7}
    >>> weights = lambda x, y: wgt.get((x, y), float('inf'))
    >>> vertices = sorted(adjacencies.keys())
    >>> source = 's'
    >>> negative_cycle, value, solution = bellman_ford(vertices,
    ...                                                out_adjacencies,
    ...                                                weights,
    ...                                                source)
    >>> negative_cycle
    False
    >>> value == {'s': 0, 't': 2, 'x': 4, 'y': 7, 'z': -2}
    True
    >>> solution == {'s': None, 't': 'x', 'x': 'y', 'y': 's', 'z': 't'}
    True

    Example with negative-cost cycle:
    >>> adjacencies = {'s': ['t'],
    ...                't': ['u'],
    ...                'u': ['v'],
    ...                'v': ['s']}
    >>> out_adjacencies = lambda x: sorted(adjacencies[x])
    >>> wgt = {('s', 't'): -2,
    ...        ('t', 'u'): -2,
    ...        ('u', 'v'): -2,
    ...        ('v', 's'): -2}
    >>> weights = lambda x, y: wgt.get((x, y), float('inf'))
    >>> vertices = sorted(adjacencies.keys())
    >>> source = 's'
    >>> negative_cycle, value, solution = bellman_ford(vertices,
    ...                                                out_adjacencies,
    ...                                                weights,
    ...                                                source)
    >>> negative_cycle
    True
    
    """
    d = {}
    predecessor = {}
    vertex_count = 0
    for v in vertex_ids:
        vertex_count += 1
        d[v] = float('inf')
        predecessor[v] = None
    d[source] = 0
    for i in range(1, vertex_count):
        for u in vertex_ids:
            for v in out_adjacencies(u):
                challenge = d[u] + weight(u, v)
                if d[v] > challenge:
                    d[v] = challenge
                    predecessor[v] = u
    for u in vertex_ids:
        for v in out_adjacencies(u):
            if d[v] > d[u] + weight(u, v):
                return True, d, predecessor
    return False, d, predecessor


# All-Pairs Shortest Path (APSP)

def johnson(vertex_ids, out_adjacencies, weight):
    """Run Johnson APSP algorithm on graph.
    
    Args:
      vertex_ids      - iterable
      out_adjacencies - callable that takes a vertex id and returns
                        an iterable of vertex ids
      weight          - callable that takes two vertex ids and
                        returns a float
      
    Returns: a 3-tuple
       boolean - does graph contain a negative cycle?
       dict - destination: path_length
       dict - destination: predecessor
       
    Example (from CLRS Chapter 25):
    >>> adjacencies = {1: [2, 3, 5],
    ...                2: [4, 5],
    ...                3: [2],
    ...                4: [1, 3],
    ...                5: [4]}
    >>> out_adjacencies = lambda x: sorted(adjacencies[x])
    >>> wgt = {(1, 2): 3,
    ...        (1, 3): 8,
    ...        (1, 5): -4,
    ...        (2, 4): 1,
    ...        (2, 5): 7,
    ...        (3, 2): 4,
    ...        (4, 1): 2,
    ...        (4, 3): -5,
    ...        (5, 4): 6}
    >>>
    >>> weights = lambda x, y: wgt.get((x, y), float('inf')) if x != y else 0
    >>> vertices = sorted(adjacencies.keys())
    >>> negative_cycle, value, sol = johnson(vertices,
    ...                                      out_adjacencies,
    ...                                      weights)
    >>> negative_cycle
    False
    >>> value == {1: {1: 0, 2: 1, 3: -3, 4: 2, 5: -4},
    ...           2: {1: 3, 2: 0, 3: -4, 4: 1, 5: -1},
    ...           3: {1: 7, 2: 4, 3: 0, 4: 5, 5: 3},
    ...           4: {1: 2, 2: -1, 3: -5, 4: 0, 5: -2},
    ...           5: {1: 8, 2: 5, 3: 1, 4: 6, 5: 0}}
    True
    >>> sol == {1: {1: None, 2: 3, 3: 4, 4: 5, 5: 1},
    ...         2: {1: 4, 2: None, 3: 4, 4: 2, 5: 1},
    ...         3: {1: 4, 2: 3, 3: None, 4: 2, 5: 1},
    ...         4: {1: 4, 2: 3, 3: 4, 4: None, 5: 1},
    ...         5: {1: 4, 2: 3, 3: 4, 4: 5, 5: None}}
    True
    
    """
    extra_vertex = '<extra_vertex>'
    vertex_ids_prime = list(vertex_ids)
    vertex_ids_prime.append(extra_vertex)
    out_adjacencies_prime = lambda u: vertex_ids if u == extra_vertex \
                                      else out_adjacencies(u)
    weight_prime = lambda u, v: 0 if u == extra_vertex else weight(u, v)
    neg, val, sol = bellman_ford(vertex_ids_prime,
                                 out_adjacencies_prime,
                                 weight_prime,
                                 extra_vertex)
    if neg:
        return neg, val, sol
    h = {}
    values = {}
    predecessors = {}
    weight_mod = lambda u, v: weight(u, v) + val[u] - val[v]
    for u in vertex_ids:
        values[u] = {}
        value, predecessor = dijkstra(vertex_ids,
                                      out_adjacencies,
                                      weight_mod,
                                      u)
        predecessors[u] = predecessor
        for v in vertex_ids:
            values[u][v] = value[v] + val[v] - val[u]
    return False, values, predecessors


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
