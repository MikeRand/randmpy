import heapq

TASKS = {0: [10, 0],
         1: [5, 1],
         2: [11, 2]}

Q = []
heapq.heapify(Q)

for i, task in TASKS.items():
    heapq.heappush(Q, task)
    
while Q:
    print(heapq.heappop(Q))
