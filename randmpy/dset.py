"""Data structures"""

import math


class DisjointSet:
    """Disjoint set (CLRS 3e chapter 21)"""

    def __init__(self):
        self.p = {}
        self.rank = {}

    def make_set(self, x):
        self.p[x] = x
        self.rank[x] = 0

    def union(self, x, y):
        self.link(self.find_set(x), self.find_set(y))
        
    def link(self, x, y):
        if self.rank[x] > self.rank[y]:
            self.p[y] = x
        else:
            self.p[x] = y
            if self.rank[x] == self.rank[y]:
                self.rank[y] += 1
                
    def find_set(self, x):
        xp = self.p[x]
        if x != xp:
            xp = self.find_set(xp)
        return xp
