"""General local search."""

import time

# ADD LOGGING

def first_improvement(neighborhood):
    for move, delta in neighborhood:
        if delta < 0:
            return move, delta
    return None, float('inf')


def best_improvement(neighborhood):
    best_move = None
    best_delta = float('inf')
    for move, delta in neighborhood:
        if delta < best_delta:
            best_move = move
            best_delta = delta
    return best_move, best_delta


class LocalSearch:
    """LocalSearch is a callable.

    __init__ args:
      improve: neighborhood iterable -> move, delta
      neighborhood: problem, solution, context -> (move, delta) iterable
    
    __call__ args:
      problem
      solution
      context

    """
    def __init__(self, neighborhood, improve, logger=None):
        self.neighborhood = neighborhood
        self.improve = improve
        self.logger = logger

    def __call__(self, problem, initial, context):
        context['ls_counter'] = 0
        s = initial
        if self.logger is not None:
            self.logger(problem, s, context, None, None)
        while True:
            context['ls_counter'] += 1
            move, delta = self.improve(self.neighborhood(problem, s, context))
            if delta >= 0:
                break
            s.update_for(move, context)
            if self.logger is not None:
                self.logger(problem, s, context, move, delta)
        assert s.satisfied
        return s, False


class IteratedLocalSearch:

    def __init__(self, search, terminate, perturb, accept, logger=None):
        """
        search: problem, solution, context -> solution, is_optimal
        terminate: problem, solution, context -> boolean
        perturb: problem, solution, context -> solution
        accept: problem, solution, solution, context -> solution

        """
        self.search = search
        self.terminate = terminate
        self.perturb = perturb
        self.accept = accept
        self.logger = logger
        
    def __call__(self, problem, initial, context):
        context['ils_counter'] = 0
        context['ils_start'] = time.time()
        s = initial
        s_hat, is_optimal = self.search(problem, initial, context)
        if self.logger is not None:
            self.logger(problem, s_hat, context)
        while not self.terminate(problem, s_hat, context) and not is_optimal:
            context['ils_counter'] += 1
            s_prime = self.perturb(problem, s_hat, context)
            s_prime_hat, is_optimal = self.search(problem, s_prime, context)
            s_hat = self.accept(problem, s_hat, s_prime_hat, context)
            if self.logger is not None:
                self.logger(problem, s_hat, context)
        assert s_hat.satisfied
        return s_hat, is_optimal
